% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/utility_functions.R
\name{plot_matrix}
\alias{plot_matrix}
\title{Plots a matrix}
\usage{
plot_matrix(m, ...)
}
\arguments{
\item{m}{The matrix}
}
\description{
Plots a matrix
}

